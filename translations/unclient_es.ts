<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../forms/multiappdialog.ui" line="20"/>
        <source>Software Update Manager</source>
        <translation>Administrador de Actualización de Software</translation>
    </message>
    <message>
        <location filename="../forms/multiappdialog.ui" line="182"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="../forms/multiappdialog.ui" line="133"/>
        <source>Check</source>
        <translation>Checar</translation>
    </message>
    <message>
        <location filename="../forms/multiappdialog.ui" line="114"/>
        <source>Install</source>
        <translation>Instalar</translation>
    </message>
</context>
<context>
    <name>Helpdialog</name>
    <message>
        <location filename="../forms/helpdialog.ui" line="42"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
</context>
<context>
    <name>MultiAppDialog</name>
    <message>
        <location filename="../src/multiappdialog.cpp" line="113"/>
        <location filename="../src/multiappdialog.cpp" line="164"/>
        <source>Ignored</source>
        <translation>Ignorado</translation>
    </message>
    <message>
        <location filename="../src/multiappdialog.cpp" line="165"/>
        <source>Don&apos;t ignore update</source>
        <translation>No ignore actualización</translation>
    </message>
    <message>
        <location filename="../src/multiappdialog.cpp" line="167"/>
        <location filename="../src/multiappdialog.cpp" line="174"/>
        <source>Ignore update</source>
        <translation>Ignorar actualización</translation>
    </message>
    <message>
        <location filename="../src/multiappdialog.cpp" line="187"/>
        <source> - Update Manager</source>
        <translation> - Administrador de Actualizaciones</translation>
    </message>
    <message>
        <location filename="../src/multiappdialog.cpp" line="220"/>
        <source>Software Update Manager</source>
        <translation>Administrador de Actualización de Software</translation>
    </message>
    <message>
        <location filename="../src/multiappdialog.cpp" line="290"/>
        <location filename="../src/multiappdialog.cpp" line="297"/>
        <source> (Size: %1)</source>
        <translation> (Tamaño: %1)</translation>
    </message>
    <message>
        <location filename="../src/multiappdialog.cpp" line="326"/>
        <source>There is a new software update available</source>
        <translation>Hay una nueva actualización de software disponible</translation>
    </message>
    <message>
        <location filename="../src/multiappdialog.cpp" line="328"/>
        <source>There are %1 new software updates available</source>
        <translation>Hay un %1 de nuevas actualización de software disponibles</translation>
    </message>
    <message>
        <location filename="../src/multiappdialog.cpp" line="330"/>
        <source>Your software is up to date</source>
        <translation>Su software está al día</translation>
    </message>
    <message>
        <location filename="../src/multiappdialog.cpp" line="386"/>
        <source>Downloading update %1 ...</source>
        <translation>Bajando Actualización %1 ...</translation>
    </message>
    <message>
        <location filename="../src/multiappdialog.cpp" line="399"/>
        <source>All updates have been installed successfully</source>
        <translation>Todas las actualizaciones se han instalado con exito</translation>
    </message>
    <message>
        <location filename="../src/multiappdialog.cpp" line="445"/>
        <source>Installing update &quot;%1&quot;</source>
        <translation>Instalando actualización &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/multiappdialog.cpp" line="451"/>
        <source>Update failed:&lt;br&gt;%1</source>
        <translation>Error en la actualización:&lt;br&gt;%1</translation>
    </message>
    <message>
        <location filename="../src/multiappdialog.cpp" line="451"/>
        <location filename="../src/multiappdialog.cpp" line="458"/>
        <source>Unable to execute the command!</source>
        <translation>¡Incapaz de ejecutar el comando!</translation>
    </message>
    <message>
        <location filename="../src/multiappdialog.cpp" line="486"/>
        <source>Update &apos;%1&apos; installed successfully</source>
        <translation>Actualización &apos;%1&apos; instalada con exito</translation>
    </message>
    <message>
        <location filename="../src/multiappdialog.cpp" line="506"/>
        <source>Update &apos;%1&apos; failed with error %2</source>
        <translation>Actualización &apos;%1&apos; Falló con error %2</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/application.cpp" line="343"/>
        <source>Unable to launch &apos;%1&apos;</source>
        <translation>Incapaz de iniciar &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/application.cpp" line="121"/>
        <source>Checking for messages ...</source>
        <translation>Buscando mensajes ...</translation>
    </message>
    <message>
        <location filename="../src/application.cpp" line="123"/>
        <source>Checking for updates ...</source>
        <translation>Buscando actualizaciones ...</translation>
    </message>
    <message>
        <location filename="../src/systemtray.cpp" line="58"/>
        <source>Launch Update Client</source>
        <translation>Iniciar Cliente de Actualización</translation>
    </message>
    <message>
        <location filename="../src/systemtray.cpp" line="63"/>
        <source>Read Messages</source>
        <translation>Leer Mensajes</translation>
    </message>
    <message>
        <location filename="../src/singleappdialog.cpp" line="262"/>
        <location filename="../src/systemtray.cpp" line="67"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="../src/updatenode_service.cpp" line="345"/>
        <location filename="../src/updatenode_service.cpp" line="372"/>
        <source>There are no new updates &amp; messages available</source>
        <translation>No hay nuevas actualizaciones &amp; mensajes disponibles</translation>
    </message>
    <message>
        <location filename="../src/updatenode_service.cpp" line="348"/>
        <location filename="../src/updatenode_service.cpp" line="375"/>
        <source>There are new updates available</source>
        <translation>Hay nuevas actualizaciones disponibles</translation>
    </message>
    <message>
        <location filename="../src/updatenode_service.cpp" line="351"/>
        <location filename="../src/updatenode_service.cpp" line="378"/>
        <source>There are new messages available</source>
        <translation>Hay nuevos mensajes disponibles</translation>
    </message>
    <message>
        <location filename="../src/updatenode_service.cpp" line="354"/>
        <location filename="../src/updatenode_service.cpp" line="381"/>
        <source>There are updates and messages available</source>
        <translation>Hay nuevas actualizaciones y mensajes disponibles</translation>
    </message>
    <message>
        <location filename="../src/updatenode_service.cpp" line="357"/>
        <location filename="../src/updatenode_service.cpp" line="384"/>
        <source>Undefined state</source>
        <translation>Estado indefinido</translation>
    </message>
    <message>
        <location filename="../src/maccommander.cpp" line="36"/>
        <source> wants to make changes.</source>
        <translation> quiere hacer cambios.</translation>
    </message>
</context>
<context>
    <name>SingleAppDialog</name>
    <message>
        <location filename="../forms/singleappdialog.ui" line="28"/>
        <source>Initalizing ...</source>
        <translation>Iniciando ...</translation>
    </message>
    <message>
        <location filename="../forms/singleappdialog.ui" line="58"/>
        <source>Show details</source>
        <translation>Mostrar detalles</translation>
    </message>
    <message>
        <location filename="../forms/singleappdialog.ui" line="71"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/singleappdialog.cpp" line="90"/>
        <source>Downloading update ...</source>
        <translation>Bajando actualización ...</translation>
    </message>
    <message>
        <location filename="../src/singleappdialog.cpp" line="92"/>
        <source>Downloading updates</source>
        <translation>Bajando actualizaciones</translation>
    </message>
    <message>
        <location filename="../src/singleappdialog.cpp" line="108"/>
        <source>Installing update ...</source>
        <translation>Instalando actualización ...</translation>
    </message>
    <message>
        <location filename="../src/singleappdialog.cpp" line="127"/>
        <source> - Update Client</source>
        <translation> - Actualizar Cliente</translation>
    </message>
    <message>
        <location filename="../src/singleappdialog.cpp" line="234"/>
        <source>Update &apos;%1&apos; installed successfully</source>
        <translation>Actualización &apos;%1&apos; instalado con exito</translation>
    </message>
    <message>
        <location filename="../src/singleappdialog.cpp" line="244"/>
        <source>Update &apos;%1&apos; failed with error %2</source>
        <translation>Actualización &apos;%1&apos; falló con error %2</translation>
    </message>
    <message>
        <location filename="../src/singleappdialog.cpp" line="252"/>
        <source>Update &apos;%1&apos; closed unexpected</source>
        <translation>Actualización &apos;%1&apos; se cerró de forma inesperada</translation>
    </message>
</context>
<context>
    <name>UpdateNode::Commander</name>
    <message>
        <location filename="../src/commander.cpp" line="236"/>
        <source>Installing update &apos;%1&apos;</source>
        <translation>Instalando actualización&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/commander.cpp" line="319"/>
        <source>Error: Update &apos;%1&apos; failed to start</source>
        <translation>Error: Actualización &apos;%1&apos; falló al iniciar</translation>
    </message>
</context>
<context>
    <name>UpdateNode::SystemTray</name>
    <message>
        <location filename="../src/systemtray.cpp" line="77"/>
        <source>Software Update Manager</source>
        <translation>Administrador de Actualización de Software</translation>
    </message>
</context>
<context>
    <name>UserMessages</name>
    <message>
        <location filename="../forms/usermessages.ui" line="99"/>
        <source>Mark as read</source>
        <translation>Marcar como leído</translation>
    </message>
    <message>
        <location filename="../forms/usermessages.ui" line="141"/>
        <source>Not Now</source>
        <translation>Ahora no</translation>
    </message>
    <message>
        <location filename="../src/usermessages.cpp" line="139"/>
        <location filename="../src/usermessages.cpp" line="178"/>
        <source> - Message</source>
        <translation> - Mensaje</translation>
    </message>
    <message>
        <location filename="../src/usermessages.cpp" line="141"/>
        <source> - Messages</source>
        <translation> - Mensajes</translation>
    </message>
    <message>
        <location filename="../src/usermessages.cpp" line="179"/>
        <source>Yes</source>
        <translation>Sí</translation>
    </message>
    <message>
        <location filename="../src/usermessages.cpp" line="180"/>
        <source>Not now</source>
        <translation>Ahora no</translation>
    </message>
    <message>
        <location filename="../src/usermessages.cpp" line="181"/>
        <source>There is a new message available:&lt;br&gt;&lt;br&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br&gt;&lt;br&gt;Do you want to read this message in your standard browser now?</source>
        <translation>Hay un nuevo mensaje disponible:&lt;br&gt;&lt;br&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br&gt;&lt;br&gt;¿Quiere leer este mensaje en su navegador actual?</translation>
    </message>
    <message>
        <location filename="../src/usermessages.cpp" line="212"/>
        <source>Loading %p% ...</source>
        <translation>Cargando %p% ...</translation>
    </message>
    <message>
        <location filename="../src/usermessages.cpp" line="228"/>
        <source>Mark as read and close</source>
        <translation>Marcar como leído y cerrar</translation>
    </message>
</context>
<context>
    <name>UserMessagesEx</name>
    <message>
        <location filename="../forms/usermessages_ex.ui" line="76"/>
        <source>Mark as read</source>
        <translation>Marcar como leído</translation>
    </message>
    <message>
        <location filename="../forms/usermessages_ex.ui" line="118"/>
        <source>Not Now</source>
        <translation>Ahora no</translation>
    </message>
    <message>
        <location filename="../forms/usermessages_ex.ui" line="131"/>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
</context>
<context>
    <name>UserNotofication</name>
    <message>
        <location filename="../forms/usernotofication.ui" line="63"/>
        <source>A new software update is available</source>
        <translation>Hay una nueva actualizacion de software</translation>
    </message>
    <message>
        <location filename="../forms/usernotofication.ui" line="76"/>
        <source>Do you want to download and install the update now?</source>
        <translation>¿Quiere bajar e instalar la actualización ahora?</translation>
    </message>
    <message>
        <location filename="../forms/usernotofication.ui" line="125"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../forms/usernotofication.ui" line="130"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <location filename="../forms/usernotofication.ui" line="135"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../forms/usernotofication.ui" line="171"/>
        <location filename="../src/usernotofication.cpp" line="146"/>
        <source>Show Details</source>
        <translation>Mostrar Detalles</translation>
    </message>
    <message>
        <location filename="../forms/usernotofication.ui" line="194"/>
        <source>Not Now</source>
        <translation>Ahora no</translation>
    </message>
    <message>
        <location filename="../forms/usernotofication.ui" line="204"/>
        <source>Continue</source>
        <translation>Continuar</translation>
    </message>
    <message>
        <location filename="../src/usernotofication.cpp" line="76"/>
        <source> - Update Client</source>
        <translation> - Actualizar Cliente</translation>
    </message>
    <message>
        <location filename="../src/usernotofication.cpp" line="153"/>
        <source>Hide Details</source>
        <translation>Ocultar detalles</translation>
    </message>
</context>
</TS>
